const colors = {
    blue: { name: 'Blue', hex: '#0000ff' },
    green: { name: 'Green', hex: '#00ff00' },
    purple: { name: 'Green', hex: '#3d34a5' },
    red: { name: 'Red', hex: '#ff0000' },
    yellow: { name: 'Yellow', hex: '#ffff00' },
};

export default colors;
