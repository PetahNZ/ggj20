<?php

use Repainter\Http\Controllers\HomeController;

Route::get('/', [HomeController::class, 'index']);
